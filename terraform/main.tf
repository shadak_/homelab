terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.9.3"
    }
  }
}

provider "proxmox" {
  pm_api_url = "px01.management.local:8006/api2/json"
}

module "primary_nameserver" {
  source = "./modules/nameserver"
  hostname = "ns01"
  vmid = "1041"
  network = {
    bridge = "vmbr0"
    ip = "10.20.10.41/24"
    gw = "10.20.10.1"
  }
  nameserver = "10.20.10.1,8.8.8.8"
  ssh_public_keys = var.management_keys
}

module "secondary_nameserver" {
  source = "./modules/nameserver"
  hostname = "ns02"
  vmid = "1042"
  network = {
    bridge = "vmbr0"
    ip = "10.20.10.42/24"
    gw = "10.20.10.1"
  }
  nameserver = "10.20.10.1,8.8.8.8"
  ssh_public_keys = var.management_keys
}

module "prometheus_monitor" {
  source = "./modules/monitor"
  hostname = "mon01"
  vmid = "1051"
  network = {
    bridge = "vmbr0"
    ip = "10.20.10.51/24"
    gw = "10.20.10.1"
  }
  nameserver = "10.20.10.1,8.8.8.8"
  ssh_public_keys = var.management_keys
}

module "loki_monitor" {
  source = "./modules/monitor"
  hostname = "mon02"
  vmid = "1052"
  network = {
    bridge = "vmbr0"
    ip = "10.20.10.52/24"
    gw = "10.20.10.1"
  }
  nameserver = "10.20.10.1,8.8.8.8"
  ssh_public_keys = var.management_keys
}

module "grafana_monitor" {
  source = "./modules/monitor"
  hostname = "mon03"
  vmid = "1053"
  network = {
    bridge = "vmbr0"
    ip = "10.20.10.53/24"
    gw = "10.20.10.1"
  }
  nameserver = "10.20.10.1,8.8.8.8"
  ssh_public_keys = var.management_keys
}


module "nebula_lighthouse" {
  source = "./modules/nebula"
  hostname = "lh01"
  vmid = "1061"
  network = {
    bridge = "vmbr0"
    ip = "10.20.10.42/24"
    gw = "10.20.10.1"
  }
  nameserver = "10.20.10.1,8.8.8.8"
  ssh_public_keys = var.management_keys
}

