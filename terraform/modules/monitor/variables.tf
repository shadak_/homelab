variable hostname {
  type        = string
  default     = ""
}

variable vmid {
  type        = number
  default     = 0
}

variable network {
  type        = object({
    bridge      = string
    ip          = string
    gw          = string
  })
  default     = {
    bridge      = ""
    ip          = ""
    gw          = ""
  }
}

variable nameserver {
  type        = string
  default     = ""
}

variable ssh_public_keys {
  type        = string
  default     = ""
}
