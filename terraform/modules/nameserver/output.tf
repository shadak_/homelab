output ip {
  value       = replace(proxmox_lxc.nameserver.network[0].ip, "/${"/"}.*$/", "")
  sensitive   = false
  description = "IP Address"
  depends_on  = []
}
